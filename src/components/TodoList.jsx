import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;

  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>
          {/* TODO show the list here 
            On créé un nouveau tableau en mapant sur le tableau existant todos
            On boucle sur chaque tâche et l'index de la tâche
            On retourne le composant TodoItem qui prend pour clé l'id de chaque tâche et qui la rajoute au tableau avec la méthode spread
          */}
          {todos.map(todo => <TodoItem key={todo.id}{...todo} />)}
      </ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
